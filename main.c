#include "bitmap.h"
#include <stdio.h>
#include <stdlib.h>

#define XSIZE 2560
#define YSIZE 2048

/// resizes image to double the size
/// by doubling each color channel
/// returns a new buffer
/// caller is responsible for freeing the buffer
uchar *resize(uchar *array, int x, int y) {
    uchar *res = calloc(x * y * 3 * 4, 1);

    for (int i = 0; i < y * 2; i++) {
        for (int j = 0; j < x * 2; j++) {
            for (int c = 0; c < 3; c++) {
                res[3 * (j + 2 * i * x) + c] = array[3 * (j / 2 + (i / 2) * x) + c];
            }
        }
    }

    return res;
}

/// inverts each color char in the buffer.
void invert_color(uchar *buffer, int image_size) {
    for (int c = 0; c < image_size; c++)
        buffer[c] -= 255;
}

int main() {
    uchar *image = calloc(XSIZE * YSIZE * 3, 1);
    readbmp("before.bmp", image);
    invert_color(image, XSIZE * YSIZE * 3);
    uchar *resized = resize(image, XSIZE, YSIZE);
    savebmp("after.bmp", resized, XSIZE * 2, YSIZE * 2);
    free(resized);
    free(image);
    return 0;
}
